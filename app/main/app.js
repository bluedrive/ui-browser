'use strict';

angular.module('bluedrive', ['ngRoute', 'bluedrive.cabinet', 'extras'])

.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/login/', {
            controller: 'LoginController',
            templateUrl: '/main/pages/login.html',
        })
        .when('/invite/', {
            controller: 'CabinetInviteController',
            templateUrl: '/main/pages/cabinet_invite.html',
        })
        .otherwise({
            templateUrl: '/main/pages/not_found.html',
        });
    $locationProvider.html5Mode(true);
}])
.run(['$rootScope', 'userManager', 'userTracking', 'tours',
        function ($rootScope, userManager, userTracking, tours) {
    userManager.check_login.then(function (user) {
        $rootScope.$broadcast('$loginStateChange', user);
    });
    $rootScope.$on('$routeChangeSuccess', function (event, current) {
        var tour_name = current.$$route && current.$$route.tour;
        $rootScope.current_tour = (!tour_name || !_.isString(tour_name)) ? tour_name : tours.get(tour_name);
        if ($rootScope.user) {
            userTracking.update();
        }
    });
    $rootScope.current = {};
    $rootScope.logout = function () {
        userManager.logout();
        $rootScope.$broadcast('$loginStateChange', false);
    };
}])
.controller('AppCtrl', ['$scope', '$location', 'data', '$rootScope', '$route', '$firebase', 'tours', '$window', '$q',
        function ($scope, $location, data, $rootScope, $route, $firebase, tours, $window, $q) {
    var build_path = function (workspace_url, folder_url) {
            var path = '/s/';
            if (workspace_url) {
                path += workspace_url + '/';
            }
            if (folder_url) {
                path += folder_url + '/';
            }
            return path
        },
        update_base_path = function (folder) {
            if (!$rootScope.workspace) return;
            $rootScope.path_base = build_path($rootScope.workspace.url, folder);
        },
        change_workspace = function (workspace) {
            var new_workspace;
            if (_.isString(workspace)) {
                change_workspace(_.findWhere($scope.workspaces, {url: workspace}));
            } else if (workspace) {
                $rootScope.user.$child('last_workspace').$set(workspace.url);
                $rootScope.workspace = workspace;
                update_base_path($rootScope.current.folder);
            } else if (user) {
                if ($rootScope.path_base) return;
                new_workspace = _.findWhere($scope.workspaces, {url: user.last_workspace});
                if (!new_workspace) {
                    new_workspace = $scope.workspaces[0];
                }
                change_workspace(new_workspace);
            } else {
                $rootScope.workspace = undefined;
                $rootScope.path_base = undefined;
            }
        },
        load_workspaces = function () {
            var loaded = $q.defer(),
                workspace_updated = _.after(_.keys(user.workspaces).length, function () {
                loaded.resolve();
            });
            $scope.workspaces = _.map(user.workspaces, function (key, url) {
                var workspace_data = {url: url};
                data.get_workspace_by_url(url).then(function (workspace) {
                    workspace_data.details = $firebase(workspace.details_ref);
                    workspace_data.ab = $firebase(workspace.ab_ref);
                    workspace_data.hack = workspace;
                    workspace_updated();
                });
                return workspace_data;
            });
            return loaded.promise;
        },
        user;
    $scope.$on('$loginStateChange', function (event, user_ref) {
        if (!user_ref) {
            $rootScope.user = undefined;
            $scope.workspaces = undefined;
            $rootScope.current = {};
            user = undefined;
            if (!$scope.loaded && $location.path() != '/login/') {
                $location.search({next: $location.url()});
            }
            $location.path('/login/');
            $scope.loaded = true;
        } else {
            $rootScope.user = $firebase(user_ref);
            user_ref.once('value', function (snap) {
                if ($location.search().next) {
                    $location.path($location.search().next);
                    $location.search('next', undefined);
                }
                user = snap.val();
                load_workspaces().then(function () {
                    change_workspace();
                    if ($location.path() == '/') {
                        $location.path(build_path($scope.workspace.url));
                    }
                    if (!user.seen_intro) {
                        tours.get('intro').start();
                        user_ref.child('seen_intro').set(true);
                    }
                    $scope.loaded = true;
                });
            });
        }
    });

    $rootScope.$watch('current.folder', update_base_path);
    $rootScope.set_workspace = change_workspace;
    $rootScope.switch_folder = function (folder) {
        if (!$scope.workspace) return;
        var path = build_path($scope.workspace.url, folder),
            route_locals = $route.current.locals;
        if (route_locals) {
            if (route_locals.type) {
                path += route_locals.type.location;
            } else if (route_locals.drawer) {
                path += route_locals.drawer.location;
            }
        }
        _.each(['add', 'all', 'renewals', 'responsibilities'], function (route) {
            if ($location.path().endsWith(route + '/')) {
                path += route + '/' + $window.location.search;
            }
        });
        return path;
    };

    $scope.$on('show_sidebar', function (event, show) {
        $scope.show_menu = show;
    });
},])
.controller('breadcrumb', ['$scope', 'types', function ($scope, types) {
    $scope.drawers = types.get_drawers();
}])
//hack
.config(['cabinetRouteProvider', function (cabinetRouteProvider) {
    cabinetRouteProvider
        .when('', {
            templateUrl: '/cabinet/pages/overview.html',
            controller: 'CabinetOverview',
            tour: 'intro',
        });
}]);
