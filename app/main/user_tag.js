'use strict';

angular.module('bluedrive')

.directive('userTag', ['$rootScope', function ($rootScope) { return {
    scope: {
        user_ws: '=userTag',
    },
    templateUrl: '/main/user_tag.html',
    link: function (scope) {
        scope.$watch('user_ws.info', function (value) {
            scope.user = value;
        });
        scope.path_base = $rootScope.path_base;
    },
};}]);
