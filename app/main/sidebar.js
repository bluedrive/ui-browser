'use strict';

angular.module('bluedrive')

.controller('Sidebar', ['$scope', '$window', 'data', 'userTracking',
    function ($scope, $window, data, userTracking) {
        $scope.current = {};

        $scope.create_cabinet = function () {
            var cabinet_name = prompt("Please enter the cabinet's name");
            if (cabinet_name) {
                data.create_cabinet(cabinet_name).then(function (url) {
                    userTracking.event.cabinet_created();
                    $window.location.href = '/s/' + url + '/settings/';//hack
                });
            }
        };
}]);
