'use strict';

angular.module('bluedrive')

.controller('CabinetInviteController', ['userManager', 'userTracking', '$scope', '$location', 'data', function (userManager, userTracking, $scope, $location, data) {
    var cabinet_id = $location.search().cabinet;

    data.get_workspace_by_id(cabinet_id).details.then(function (details) {
        $scope.cabinet = details;
    });
    $scope.add = function () {
        userManager.add_cabinet(cabinet_id);
        if (cabinet_id == '-JLyHBEjsXJ9kliUC3nw') {
            userTracking.event('added-star-doors');
        }
        userTracking.event.accept_cabinet_invite();
        $location.path('/s/' + cabinet_id + '/').search('cabinet', null);
    };
}]);
