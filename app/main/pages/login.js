'use strict';

angular.module('bluedrive')

.controller('LoginController', ['userManager', '$scope', '$location', function (userManager, $scope, $location) {
    $scope.next_url = $location.search().next;
    $scope.login = function () {
        $scope.login_active = true;
        $scope.login_failed = false;
        userManager.login().then(
            function (user) {
                var next_url = $scope.next_url;
                $location.url(next_url || '/');
                $scope.$emit('$loginStateChange', user);
            },
            function () {
                $scope.login_active = false;
                $scope.login_failed = true;
            }
        );
    };
}]);
