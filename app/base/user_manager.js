"use strict";

angular.module('bluedrive.base')

.factory('userManager', ['$q', 'database', 'userTracking',
               function ( $q, database, userTracking) {
    var get_cabinet = function (key) {
            var cabinets = database.child('data');
            return key ? cabinets.child(key) : cabinets;
        },
        beta_cabinet = get_cabinet('-JLKDxRHyAy4lr6qErAW'),
        get_db_user = function (key) {
            return database.child('users').child(key);
        },
        deferred_login = $q.defer(),
        user_ref, user_id,
        refresh_workspaces = function (user_data) {
            user_ref.child('workspaces').once('value', function (snap) {
                var workspaces = snap.val();
                _.each(workspaces, function (workspace) {
                    var workspace_ref = get_cabinet(workspace),
                        workspace_user_ref = workspace_ref.child('users').child(user_id);
                    workspace_user_ref.child('info').update(user_data);
                });
            });
        },
        auth = FirebaseSimpleLogin(database, function (error, user) {
            if (user) {
                user_id = user.id;
                user_ref = get_db_user(user.id);
                user_ref.once('value', function (value) {
                    var intercom_data = {
                        email: user.email,
                        user_id: user_id
                    };
                    _.extend(intercom_data, _.omit(user.thirdPartyUserData, ['id',]));
                    userTracking.boot(intercom_data);
                    var user_data = {
                        id: user.id,
                        email: user.email,
                        name: user.name || '',
                        extra: user.thirdPartyUserData
                    };
                    database.child('emails').child(user.id).set(user.email);
                    if (value.val()) {
                        user_ref.child('info').update(user_data);
                        refresh_workspaces(user_data);
                        deferred_login.resolve(user_ref);
                    } else {
                        user_ref.child('info').set(user_data);
                        beta_cabinet.once('value', function (snap) {
                            var data = snap.val(),
                                cabinet;
                            data.users = _.pick(data.users, function (value, key) {
                                return key.indexOf('beta') === 0;
                            });
                            data.users[user_id] = {
                                write: true,
                                info: user_data,
                            };
                            cabinet = get_cabinet().push(data, function () {
                                deferred_login.resolve(user_ref);
                            });
                            userManager.add_cabinet(cabinet.name(), 'beta_cabinet');
                        });
                    }
                }, function () {
                    deferred_login.resolve(false);
                })
            } else {
                deferred_login.resolve(false);
            }
        }),
        userManager = {
            get_user_id: function () {
                return user_id;
            },
            add_cabinet: function (cabinet_id, cabinet_url) {
                var deferred = $q.defer();

                if (cabinet_url === undefined) {
                    cabinet_url = cabinet_id;
                }

                user_ref.child('workspaces').child(cabinet_url).set(cabinet_id, function () {
                    user_ref.child('info').once('value', function (snap) {
                        refresh_workspaces(snap.val());
                    });
                    deferred.resolve(cabinet_id);
                });
                return deferred.promise;
            },
            get_workspace_id_by_url: function (workspace_url) {
                var deferred = $q.defer();

                deferred_login.promise.then(function (user) {
                    if (user) {
                        user_ref.child('workspaces').child(workspace_url).on('value', function (snap) {
                            if (snap.val()) {
                                deferred.resolve(snap.val());
                            } else {
                                deferred.reject();
                            }
                        }, deferred.reject);
                    } else {
                        deferred.reject();
                    }
                }, deferred.reject)
                return deferred.promise;
            },
            check_login: deferred_login.promise,
            login: function () {
                deferred_login = $q.defer();
                auth.login('google', {rememberMe: true});
                return deferred_login.promise;
            },
            logout: function () {
                auth.logout();
            }
        };
    return userManager;
}]);
