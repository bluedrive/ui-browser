'use strict';

angular.module('templates', []);
angular.module('bluedrive.base', ['templates', 'firebase', 'ngCookies', 'angularFileUpload', 'ngRoute'])

.provider('cabinetRoute', ['$routeProvider', function ($routeProvider) {
    return _.extend({}, $routeProvider, {
        $get: angular.noop,
        when: function (route, settings) {
            var resolvers = {
                    workspace: ['data', '$route', '$rootScope', '$q', 'types',
                        function (data,   $route,   $rootScope,   $q) {
                            var deferred = $q.defer(),
                                workspace_url = $route.current.params.workspace,
                                folder = $route.current.params.folder;
                            $rootScope.set_workspace(workspace_url);
                            $rootScope.current.folder = folder;
                            data.get_workspace_by_url(workspace_url).then(function (workspace) {
                                deferred.resolve(workspace);
                            }, deferred.reject);
                            return deferred.promise;
                        }],
                },
                new_settings = _.clone(settings);

            if (!new_settings.resolve) {
                new_settings.resolve = {};
            }
            _.extend(new_settings.resolve, resolvers);
            $routeProvider.when('/s/:workspace/:folder?/' + route, new_settings);
            return this;
        }
    });
}]);