"use strict";

angular.module('bluedrive.base')

.factory('tours', [function () {
    var tours = {};

    return {
        create_tour: function (name, data) {
            var tour = new Shepherd.Tour(data);
            tours[name] = tour;
            return tour;
        },
        get: function (name) {
            return tours[name];
        }
    }
}]);
