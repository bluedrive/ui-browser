'use strict';

angular.module('bluedrive.base')

.constant('database', new Firebase('https://thebd.firebaseio.com/'))
.factory('data', ['$q', 'database', 'userManager',
        function ( $q,   database, userManager) {
    var build_key = function (name) {
            return name.replace(/[\/.$[\]# ]/g, '_');
        },
        get_workspace = function (key) {
            var workspaces = database.child('data')
            return key ? workspaces.child(key) : workspaces;
        },
        dataProvider = {
            create_cabinet: function (name) {
                var cabinet_id,
                    users = {},
                    deferred = $q.defer();
                users[userManager.get_user_id()] = {write: true};
                cabinet_id = get_workspace().push({details: {name: name}, users: users}, function () {
                    userManager.add_cabinet(cabinet_id).then(function (cabinet_url) {
                        deferred.resolve(cabinet_url);
                    });
                }).name();
                return deferred.promise;
            },
            get_workspace_by_url: function (workspace_url) {
                var deferred = $q.defer();
                userManager.get_workspace_id_by_url(workspace_url).then(function (workspace_id) {
                    deferred.resolve(dataProvider.get_workspace_by_id(workspace_id));
                }, deferred.reject);
                return deferred.promise;
            },
            get_workspace_by_id: function (workspace_id) {
                var details = $q.defer(),
                    workspace_ref = get_workspace(workspace_id),
                    workspace = {
                        get_user_list: function () {
                            var deferred = $q.defer()
                            user_ref.on('value', function (snap) {
                                if (snap.val()) {
                                    deferred.resolve(snap.val());
                                } else {
                                    deferred.reject();
                                }
                            }, deferred.reject);
                            return deferred.promise;
                        },
                        get_users: function (key) {
                            var ref = key ? user_ref.child(key):  user_ref,
                                deferred = $q.defer();

                            ref.once('value', function (snap) {
                                deferred.resolve(snap.val());
                            });

                            return deferred.promise;
                        },
                        get_user_ref: function (key) {
                            return key ? user_ref.child(key):  user_ref;
                        },
                        create_folder: function (folder) {
                            workspace.details_ref.child('folders').child(build_key(folder)).set({name: folder});
                        },
                        create_company: function (name, data) {
                            var key = build_key(name),
                                company = workspace.get_companies(key),
                                values = _.defaults({
                                    key: key,
                                    name: name,
                                }, data);
                            company.set(values);
                            _.extend(company, values);
                            return company;
                        },
                        get_companies: function (name) {
                            var companies = workspace_ref.child('companies');
                            return name ? companies.child(name) : companies;
                        },
                        ab_ref: workspace_ref.child('ab'),
                        details: details.promise,
                        details_ref: workspace_ref.child('details'),
                        links_ref: workspace_ref.child('links'),
                        get_item: function (type, key) {
                            var item_ref = workspace_ref.child('records').child('current').child(type.database_name),
                                revision_ref = workspace_ref.child('records').child('revisions').child(type.database_name);
                            if (key) {
                                item_ref = item_ref.child(key);
                                revision_ref = revision_ref.child(key);
                            }
                            item_ref.revision_ref = revision_ref;
                            return item_ref;
                        },
                        create_item: function (type, data) {
                            var type_ref = workspace.get_item(type),
                                item_ref = type_ref.push({
                                    data: data,
                                    last_modified: Firebase.ServerValue.TIMESTAMP,
                                    created: Firebase.ServerValue.TIMESTAMP,
                                });
                            item_ref = workspace.get_item(type, item_ref.name());
                            item_ref.child('key').set(item_ref.name());
                            return item_ref;
                        },
                    },
                    user_ref = workspace_ref.child('users');
                workspace.details_ref.once('value', function (snap) {
                    if (snap.val()) {
                        details.resolve(snap.val());
                    } else {
                        details.reject();
                    }
                });
                return workspace;
            }
        };
    return dataProvider;
},]);
