'use strict';

angular.module('bluedrive.technical')

.controller('sm_options', ['$scope', function ($scope) {
    $scope.network_options = {
        local: 'Facebook, Linkedin, Twitter, Google+, Youtube, Pinterest, Tumblr, Flickr, Instagram, Meetup, Slideshare, Path, Mobli, Vine, Foursquare, MySpace, Strutta, Mobio, Techvibes'.split(', '),
    };
    $scope.type_options = {
        local: 'Social network, social news, media sharing, micro blogging, blogs, forums'.split(', '),
    };
    $scope.item.$on('loaded', function (data) {
        $scope.sm_type = data.type;
        $scope.sm_network = data.network;
    });
    $scope.$watch('sm_network', function (network) {
        if (!network) return;
        $scope.item.network = network.value;
    });
    $scope.$watch('sm_type', function (type) {
        if (!type) return;
        $scope.item.type = type.value;
    });
}]);
