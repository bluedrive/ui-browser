'use strict';

angular.module('bluedrive.technical', ['ngRoute', 'ui.select2', 'templates', 'bluedrive.record'])

.config(['typesProvider', function (typesProvider) {
    typesProvider.register_drawer({
        id: 'technical',
        title: 'Technical',
        path: 'technical',
        sub: [{
            title: 'Domains',
            add_title: 'Domain',
            id: 'technical.domain',
            path: 'domains',
            database_name: 'domains',
            input_title: 'Domain Name (URL)',
            pattern: /^.+\.\w{2,6}$/,
            common_relations: ['Registrar'],
        }, {
            title: 'Hosting',
            add_title: 'Hosting',
            id: 'technical.hosting',
            path: 'hosting',
            database_name: 'hosting',
            input_title: 'Hosting Package',
            pattern: /^.+$/,
            common_relations: ['Provider'],
        }, {
            title: 'Security',
            add_title: 'Security',
            id: 'technical.ssl',
            path: 'security',
            database_name: 'ssl',
            input_title: 'Record Name',
            pattern: /^.+$/,
            common_relations: ['Provider'],
        }, {
            title: 'Software',
            add_title: 'Software',
            id: 'technical.software',
            path: 'software',
            database_name: 'software',
            input_title: 'Software Name',
            pattern: /^.+$/,
            common_relations: ['Provider'],
        }, {
            title: 'Social Media',
            add_title: 'Social Media Account',
            id: 'technical.social_media',
            path: 'social_media',
            database_name: 'social_media',
            input_title: 'Account Name',
            pattern: /^.+$/,
            common_relations: ['Provider'],
        }, {
            title: 'Compliance',
            add_title: 'Compliance Record',
            id: 'technical.compliance',
            path: 'compliance',
            database_name: 'technical_compliance',
            input_title: 'Compliance Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'Technical Meeting Minutes',
            add_title: 'Technical Meeting Name',
            id: 'technical.meeting',
            path: 'meetings',
            database_name: 'technical_meetings',
            input_title: 'Technical Meeting Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'Technical Reports',
            add_title: 'Technical Report',
            id: 'technical.report',
            path: 'reports',
            database_name: 'technical_reports',
            input_title: 'Technical Report',
            pattern: /^.+$/,
            common_relations: [''],
        },],
    });
}]);
