'use strict';

angular.module('extras')

.directive('editable', [function () { return {
    require: 'ngModel',
    scope: {
        editable: '=editable',
        placeholder: '@placeholder',
        maxlength: '@ngMaxlength',
        multiline: '@multiline',
    },
    link: function (scope, element, attrs, ngModelCtrl) {
        var empty = false,
            editable = scope.editable;
        element.on('blur', function() {
            scope.$apply(function() {
                var value,
                    max_length = parseInt(scope.maxlength, 10);
                if (scope.multiline === undefined) {
                    value = element.text();
                    value = value.replace('\n', '').replace('<br>', '');
                } else {
                    value = element.html();
                }
                empty = false;
                if (!value) {
                    empty = true;
                }
                if (value.length > max_length) {
                    value = value.substring(0, max_length);
                }
                ngModelCtrl.$setViewValue(value);
                ngModelCtrl.$render();
            });
        });

        element.on('focus', function() {
            if (empty && editable) {
                element.html('');
            }
        });

        ngModelCtrl.$render = function() {
            if (!ngModelCtrl.$viewValue) {
                if (editable) {
                    attrs.$addClass('placeholder');
                    element.html(scope.placeholder);
                } else {
                    attrs.$removeClass('placeholder');
                    element.html('');
                }
            } else {
                attrs.$removeClass('placeholder');
                element.html(ngModelCtrl.$viewValue);
            }
        };

        ngModelCtrl.$formatters.unshift(function (modelValue) {
            empty = !modelValue;
            if (modelValue && scope.multiline === undefined) {
                modelValue = modelValue.replace('\n', '').replace('<br>', '');
            }
            return modelValue;
        });

        scope.$watch('placeholder', function (placeholder) {
            if (empty) {
                attrs.$addClass('placeholder');
                element.html(placeholder);
            } else {
                attrs.$removeClass('placeholder');
            }
        })

        scope.$watch('editable', function (value) {
            element.attr('contenteditable', value);
            editable = value;
            ngModelCtrl.$render();
        });
    },
};},]);
