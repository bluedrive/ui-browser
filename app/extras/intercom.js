'use strict';

angular.module('extras')

//.constant('intercom_api_key', 'exioprb')
.constant('intercom_api_key', 'c7ebc027458b1c1c9760cc7d807088e82b3b4927')

.factory('Intercom', ['$window', function ($window) { return $window.Intercom || function () {console.log('User tracking disabled.', arguments);}; }])
.factory('userTracking', ['Intercom', 'intercom_api_key', function (Intercom, intercom_api_key) {
    var userTracking = {
            boot: function (data) {
                if (!data.app_id) {
                    data.app_id = intercom_api_key;
                }
                if (!data.widget) {
                    data.widget = { activator: '#IntercomWidget' };
                }
                Intercom('boot', data);
            },
            update: function (data) {
                Intercom('update', data);
            },
            event: function (event_name, data) {
                Intercom('trackUserEvent', event_name, data);
            }
        },
        add_event = function (name) {
            return function (data) {
                userTracking.event(name, data);
            };
        },
        events = userTracking.event;

    events.cabinet_created = add_event('created-a-cabinet');
    events.record_created = add_event('created-a-record');
    events.section_created = add_event('created-a-group');
    events.user_added_to_cabinet = add_event('added-a-user-to-a-cabinet');
    events.completed_intro_tour = add_event('completed-intro-tour');
    events.accept_cabinet_invite = add_event('accepted-cabinet-invite');

    return userTracking;
}]);
