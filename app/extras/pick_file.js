'use strict';

angular.module('extras')

.constant('DropBoxKey', 'o8iozgdjvht8kfe')
.factory('Dropbox', ['DropBoxKey', function (key) { Dropbox.appKey = key; return Dropbox; }])
.constant('GoogleKey', {
    app_id: '437294369119',
    api_key: 'AIzaSyB_WqfhkYjLlzOxrP0Vl6pDL2cGcpDQsco',
    client_id: '437294369119-77808s6f9toqd3ul2gj0r020308hoqs9.apps.googleusercontent.com',
})
.factory('GoogleAuth', ['GoogleKey', '$q', function (googleKey, $q) {
    gapi.client.setApiKey(googleKey.api_key);
    gapi.auth.authorize({
        client_id: googleKey.client_id,
        scope: 'https://www.googleapis.com/auth/drive.readonly',
        immediate: true,
    });
    return _.extend({
        get_auth: function () {
            var token = gapi.auth.getToken(),
                deferred_auth = $q.defer();
            if (token) {
                deferred_auth.resolve(token);
            } else {
                gapi.auth.authorize({
                    client_id: googleKey.client_id,
                    scope: 'https://www.googleapis.com/auth/drive.readonly',
                    immediate: false,
                    authuser:"",
                }, function () {deferred_auth.resolve(gapi.auth.getToken())});
            }
            return deferred_auth.promise;
        }
    }, googleKey);
}])
.factory('filePicker', ['$q', 'Dropbox', 'GoogleAuth', '$window', function ($q, Dropbox, GoogleAuth, $window) {
    return {
        show: function (service, options) {
            var deferred = $q.defer(),
                multiple = options && options.multiple,
                format_drive_file = function (file) {
                    file.link = file.url;
                    return file;
                };
            if (service.toLowerCase() === 'dropbox') {
                Dropbox.choose({
                    linkType: 'direct',
                    success: function(files) {
                        if (multiple) {
                            deferred.resolve(files);
                        } else {
                            deferred.resolve(files[0]);
                        }
                    }
                });
            } else if (service.toLowerCase() === 'drive') {
                GoogleAuth.get_auth().then(function (auth) {
                    gapi.load('picker', {'callback': function () {
                        var picker = new google.picker.PickerBuilder()
                            .setAppId(GoogleAuth.app_id)
                            .addView(new google.picker.DocsView()
                                .setParent('root')
                                .setIncludeFolders(true)
                                .setMode(google.picker.DocsViewMode.LIST))
                            //.addView(new google.picker.DocsUploadView()
                            //    .setIncludeFolders(true))
                            .addView(new google.picker.MapsView())
                            .addView(new google.picker.VideoSearchView())
                            .setOAuthToken(auth.access_token)
                            .setOrigin($window.location.protocol + '//' + $window.location.host)
                            .setCallback(function (event) {
                                var files;
                                if (event.action === google.picker.Action.PICKED) {
                                    files = event.docs;
                                    if (multiple) {
                                        deferred.resolve(_.map(files, format_drive_file));
                                    } else {
                                        deferred.resolve(format_drive_file(files[0]));
                                    }
                                }
                            })
                            .build();
                        if (multiple) {
                            picker.enableFeature(google.picker.Feature.MULTISELECT_ENABLED);
                        }
                        picker.setVisible(true);
                    }});
                });
            }
            return deferred.promise;
        }
    };
}]);
