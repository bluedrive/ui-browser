'use strict';

angular.module('extras')

.filter('keyedFilter', ['$filter', function ($filter) {
    return function (items, expression, comparator) {
        var results = {};
        angular.forEach(items, function (value, key) {
            if ($filter('filter')([value], expression, comparator).length > 0) {
                results[key] = value;
            }
        });
        return results;
    };
}]);
