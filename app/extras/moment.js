'use strict';

angular.module('extras')

.filter('ago', ['$filter', function ($filter) {
    return function (time, format) {
        if (moment(time).isBefore(moment().subtract('week', 1))) {
            return $filter('date')(time, format);
        }
        return moment(time).fromNow();
    };
}])
.filter('from', function () {
    return function (input, from) {
        if (!from) {
            return moment(input).fromNow();
        }
        return moment(input).from(from);
    };
})
.filter('duration', function () {
    return function (input) {
        return moment.duration(input).humanize();
    };
})
.filter('every', function () {
    return function (input) {
        return moment.duration(input).humanize().replace(/^a /, '');
    };
})
.filter('beforeNow', function () {
    return function (input) {
         return moment(input).isBefore();
    }
})
.directive('dateInput', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ngModelCtrl) {
            ngModelCtrl.$formatters.unshift(function (modelValue) {
                if (modelValue) {
                    return moment(modelValue).format('YYYY-MM-DD');
                }
            });
            ngModelCtrl.$parsers.unshift(function(viewValue) {
                return moment(viewValue, 'YYYY-MM-DD').toDate();
            });
        },
    };
})
.directive('durationInput', function() {
    return {
        require: 'ngModel',
        scope: {
            duration: '=durationInput'
        },
        link: function(scope, elm, attrs, ngModelCtrl) {
            scope.$watch('duration', function (duration) {
                ngModelCtrl.$modelValue = undefined; // force viewValue to be updated
            });
            ngModelCtrl.$formatters.unshift(function (modelValue) {
                if (scope.duration) {
                    return moment.duration(parseInt(modelValue, 10)).as(scope.duration);
                }
            });
            ngModelCtrl.$parsers.unshift(function(viewValue) {
                return moment.duration(parseInt(viewValue, 10), scope.duration).asMilliseconds();
            });
        },
    };
});
