'use strict';

angular.module('extras')

.constant('gravatar_url', 'http://www.gravatar.com/avatar/')
.directive('gravatar', ['gravatar_url', function (url) {
    return {
        restrict: 'AC',
        scope: {
            email: '@gravatar',
            size: '=size',
        },
        link: function (scope, element, attrs) {
            var update_src = function (email) {
                var hash = Crypto.MD5(email.trim().toLowerCase()),
                    extra = '?d=identicon';

                if (scope.size) {
                    extra += '&s=' + scope.size;
                }
                attrs.$set('src', url + hash + extra)
            };
            scope.$watch('email', update_src);
        },
    };
}]);
