'use strict';

angular.module('extras')

.directive('affixTop', ['$window', '$document', function ($window, $document) { return {
    link: function (scope, element) {
        var fixed_at = 0,
            update = function () {
                if ((fixed_at !== 0 ? fixed_at:element.offset().top) <= ($window.pageYOffset || $document[0].documentElement.scrollTop)) {
                    if (fixed_at === 0)
                        fixed_at = element.offset().top;
                    element.addClass('affix');
                } else {
                    fixed_at = 0;
                    element.removeClass('affix');
                }
            };
        $($window).on('load scroll', function () {
            update();
        });
        $($window).on('resize', function () {
            element.removeClass('affix');
            fixed_at = element.offset().top;
            element.addClass('affix');
            update();
        });
    },
};},]);
