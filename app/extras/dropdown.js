'use strict';

angular.module('extras')

.directive('dropdown', ['$document', function ($document) { return {
    scope: {
        dropdown_show: '&dropdown',
    },
    link: function (scope, element) {
        var tracker = false;
        element.on('click', function (event) {
            scope.dropdown_show({$show: true});
            tracker = true;
            scope.$apply();
        });
        $document.on('click', function () {
            if (!tracker) {
                scope.dropdown_show({$show: false});
                scope.$apply();
            }
            tracker = false;
        });
    },
};},]);
