'use strict';

angular.module('extras')

.directive('dateEdit', [function () { return {
    templateUrl: '/extras/date_edit.html',
    scope: {
        edit_mode: '=canEdit',
        date: '=dateEdit'
    },
};}]);
