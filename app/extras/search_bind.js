'use strict';

angular.module('extras')

.factory('searchBind', ['$location', function ($location) {
    return function (scope, name, params) {
        scope.$on('$routeUpdate', function () {
            scope.search = _.pick($location.search(), params);
        });
        scope.search = {};
        _.each(params, function (param) {
            scope.$watch(name + '.' + param, function (searchData) {
                $location.search(param, searchData);
            });
            scope.search[param] = $location.search()[param];
        });
    };
},]);
