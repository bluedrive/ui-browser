'use strict';

angular.module('bluedrive.technical')

.controller('History', ['$scope', 'workspace', 'type', '$routeParams', '$firebase',
        function ($scope, workspace, type, $routeParams, $firebase) {
    var item_ref = workspace.get_item(type, $routeParams.name);

    $scope.type = type;
    $scope.item = $firebase(item_ref);
    $scope.revisions = $firebase(item_ref.revision_ref);
    workspace.get_users().then(function (users) {
        $scope.users = users;
    });
    $scope.set_selected = function(index) {
        $scope.selected = $scope.selected==index? null:index;
    }
}]);
