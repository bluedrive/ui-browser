'use strict';

angular.module('bluedrive.record')

.controller('DetailController', ['$scope', 'workspace', 'type', '$routeParams', 'data', 'types', '$location', 'userManager', 'filePicker', '$firebase',
                        function ($scope,   workspace,   type,   $routeParams,   data,   types,   $location,   userManager,   filePicker,   $firebase) {
    var name = $routeParams.name,
        item_ref = workspace.get_item(type, name),
        current_item_ref = item_ref.child('data');

    $scope.selected = {
        companies: [],
    };

    $scope.toggle_folder = function (folder) {
        $scope.item.folders = _.values($scope.item.folders);
        var index = $scope.item.folders.indexOf(folder);
        if (index == -1) {
            $scope.item.folders.push(folder);
        } else {
            $scope.item.folders.splice(index, 1);
        }
    };

    $scope.save = function () {
        item_ref.revision_ref.transaction(function (revisions) {
            var revisions = revisions || [];
            revisions.push({
                date: Firebase.ServerValue.TIMESTAMP,
                change: 'Record edited.',
                user: userManager.get_user_id(),
            });
            return revisions;
        });

        _.each($scope.selected.companies, function (ref, index) {
            var company = ref.company;
            if (company.new) {
                company.new = null;
                company = workspace.create_company(company.name, company);
            } else {
                company.$save();
            }
            $scope.item.companies[index].company = company.key;
            if ($scope.item.companies[index].relation.value) {
                $scope.item.companies[index].relation = $scope.item.companies[index].relation.value;
            }
        });
        if ($scope.account) {
            if ($scope.account.key) {
                $scope.item.account = $scope.account.key;
            }
        } else {
            $scope.item.account = undefined;
        }
        if ($scope.account2) {
            if ($scope.account2.key) {
                $scope.item.account2 = $scope.account2.key;
            }
        } else {
            $scope.item.account2 = undefined;
        }

        $scope.item.$save();

        if ($scope.new_files.length > 0) {
            var item_files = $firebase(current_item_ref.child('files'));
            _.each($scope.new_files, function (file) {
                if (file.description) {
                    if (!/^.+:\/\//.test(file.description)) {
                        file.description = 'http://' + file.description
                    }
                    item_files.$add(_.pick(file, 'name', 'description', 'file_name', 'uploaded_by', 'uploaded_on'));
                }
            });
        }

        item_ref.child('last_modified').set(Firebase.ServerValue.TIMESTAMP);

        $scope.edit_mode = false;
        $location.search('edit', undefined);
    };
    $scope.cancel = function () {
        if ($scope.item.new) {
            $location.path('/s/' + workspace.url + '/' + type.location).search('edit', undefined);
            $scope.item.$remove();
        } else {
            $scope.item = $firebase(current_item_ref);
            $scope.edit_mode = false;
            $location.search('edit', undefined);
        }
    };

    $scope.add_company = function () {
        $scope.item.companies.push({
            relation: '',
            contact: 0,
        });
        $scope.selected.companies.push({});
    };

    $scope.type = type;
    $scope.item = $firebase(current_item_ref);
    item_ref.once('value', function (snap) {
        var item;
        if (snap.val()) {
            $scope.last_modified = snap.val().last_modified;
            item = snap.val().data;
            if (item.companies) {
                _.each(item.companies, function (company) {
                    $scope.selected.companies.push({
                        company: $firebase(workspace.get_companies(company.company)),
                    });
                });
            }
            if (item.account) {
                $scope.account = $firebase(workspace.get_user_ref(item.account));
            }
            if (item.account2) {
                $scope.account2 = $firebase(workspace.get_user_ref(item.account2));
            }
            if (!$scope.$$phase) $scope.$digest();
        }
    });
    $scope.edit = function () {
        $location.search('edit');
        $scope.new_files = [];
        $scope.edit_mode = true;
        _.defaults($scope.item, {
            expiry: {},
            options: [],
            companies: [],
            folders: [],
            notifications: {},
        });
    };

    $scope.common_relations = {
        name: 'companies',
        local: type.common_relations,
    };

    $scope.move_company = function (index, change) {
        $scope.selected.companies.splice(index + change, 0, $scope.selected.companies.splice(index, 1)[0]);
        $scope.item.companies.splice(index + change, 0, $scope.item.companies.splice(index, 1)[0]);
    };

    $scope.remove_company = function (index) {
        $scope.selected.companies.splice(index, 1);
        $scope.item.companies.splice(index, 1)
    };

    workspace.get_users().then(function (users) {
        $scope.users = users;
    });

    $scope.add_file = function (type) {
        if (type === 'link') {
            $scope.new_files.unshift({
                name: '',
                description: '',
                uploaded_by: userManager.get_user_id(),
                uploaded_on: Firebase.ServerValue.TIMESTAMP,
            });
        } else {
            filePicker.show(type).then(function (file) {
                $scope.new_files.unshift({
                    name: file.name,
                    description: file.link,
                    uploaded_by: userManager.get_user_id(),
                    uploaded_on: Firebase.ServerValue.TIMESTAMP,
                });
            });
        }
    };

    $scope.onFileSelect = function ($files) {
        $scope.new_files.unshift({
            name: $files[0].name,
            description: '',
            file_name: $files[0].name,
            uploaded_by: userManager.get_user_id(),
            uploaded_on: Firebase.ServerValue.TIMESTAMP,
            ref: $files[0],
        });
    };

    if ($location.search().edit !== undefined) {
        $scope.edit();
    }
}])
.directive('clickFileInput', [function () { return {
    link: function (scope, element) {
        element.on('click', function () {
            element.siblings('[type="file"]').trigger('click');
        });
    }
}}]);
