'use strict';

angular.module('bluedrive.record')

.controller('ListController', ['$scope', 'workspace', 'type', 'searchBind', function ($scope, workspace, type, searchBind) {
    $scope.type = type;
    $scope.loading = true;
    workspace.get_item(type).once('value', function (snap) {
        $scope.loading = false;
        $scope.items = snap.val();
        if (!$scope.$$phase) $scope.$digest();
    });
    searchBind($scope, 'search', ['text']);
}]);
