'use strict';

angular.module('bluedrive.record')

.controller('AddRecord', ['$scope', 'workspace', '$location', '$routeParams', 'data', 'types', 'userTracking', 'filePicker', 'userManager',
        function ($scope, workspace, $location, $routeParams, data, types, userTracking, filePicker, userManager) {
    var pick = function () {
            var to_pick = arguments;
            return function (item) {
                return _.map(to_pick, function (key) {
                    return item[key];
                }).join(' ');
            };
        },
        options = {};
    _.each(types, function (drawer, key) {
        if (_.isFunction(drawer) || _.isArray(drawer)) return;
        options[key] = {
            add_title: drawer.title,
            children: _.map(drawer.sub, function (value, key) {
                var item = _.clone(value);
                _.extend(item, {
                    parent_title: drawer.title,
                    parent: undefined,
                });
                return item;
            }),
        };
    });
    if ($location.search().type) {
        _.each(options, function (drawer) {
            var type = _.findWhere(drawer.children, {id: $location.search().type,});
            if (type) $scope.type = type;
        });
    }
    $scope.$watch('type', function (type) {
        $location.search('type', type && type.id);
    });
    $scope.item = {
        account: {
            key: userManager.get_user_id()
        },
        folders: $scope.current.folder ? [$scope.current.folder] : [],
    };
    $scope.toggle_folder = function (folder) {
        $scope.item.folders = _.values($scope.item.folders);
        var index = $scope.item.folders.indexOf(folder);
        if (index == -1) {
            $scope.item.folders.push(folder);
        } else {
            $scope.item.folders.splice(index, 1);
        }
    };
    $scope.file = {
        uploaded_by: userManager.get_user_id(),
    };
    $scope.add_file = function (type) {
        filePicker.show(type).then(function (file) {
            $scope.file.name = file.name;
            $scope.file.description = file.link;
        });
    };
    $scope.create = function (type, item) {
        var item_ref, file,
            item_data = {
                name: item.name,
                description: item.description || '',
                folders: item.folders,
            };
        if (item.account && item.account.key) {
            item_data.account = item.account.key;
        }
        item_ref = workspace.create_item(type, item_data);
        file = $scope.file;
        if (file && file.description) {
            if (!/^.+:\/\//.test(file.description)) {
                file.description = 'http://' + file.description
            }
            file.uploaded_on = Firebase.ServerValue.TIMESTAMP;
            item_ref.child('data').child('files').push(file);
        }
        item_ref.revision_ref.set([{
            date: Firebase.ServerValue.TIMESTAMP,
            change: 'Record created.',
            user: userManager.get_user_id(),
        }])
        userTracking.event.record_created({type: type.id});
        $location.url($scope.path_base + type.location + item_ref.name() + '/');
    };
    $scope.select2opt = {
        id: 'id',
        data: {
            results: _.values(options),
            text: pick('parent_title', 'add_title'),
        },
        formatSelection: pick('add_title'),
        formatResult: pick('add_title'),
        width: 'resolve',
    };
    $scope.selected = null;
}]);
