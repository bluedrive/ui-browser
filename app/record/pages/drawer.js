'use strict';

angular.module('bluedrive.technical')

.controller('DrawerController', ['$scope', 'drawer', '$location', function ($scope, drawer, $location) {
    $scope.drawer = drawer;
    $scope.run_search = function (url) {
        $location.url(url);
    };
}]);
