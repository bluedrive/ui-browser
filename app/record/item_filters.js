'use strict';

angular.module('bluedrive.record')

.filter('isPrimary', function () {
    return function (items, user) {
        return _.filter(items, function (item) {
            if (item && !_.isFunction(item)) {
                if (user) {
                    return item.data.account === user;
                } else if (!item.data.account) {
                    return true;
                }
            }
            return false;
        });
    }
})
.filter('isSecondary', function () {
    return function (items, user) {
        return _.filter(items, function (item) {
            if (item) {
                if (user) {
                    return item.data.account2 === user;
                } else if (!item.data.account2) {
                    return true;
                }
            }
            return false;
        });
    }
})
.filter('isCompany', function () {
    return function (items, company) {
        return _.filter(items, function (item) {
            return _.any(item.data.companies, function (value) {
                return value && value.company === company.key;
            });
        });
    }
})
.filter('inFolder', function () {
    return function (items, folder) {
        if (!folder) {
            return items;
        }
        return _.filter(items, function (item) {
            return _.any(item.data.folders, function (value) {
                return value === folder;
            });
        });
    }
});
