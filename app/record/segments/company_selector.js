'use strict';

angular.module('bluedrive.record')

.directive('companySelector', ['data', '$rootScope', '$firebase', function (data, $rootScope, $firebase) {
    return {
        scope: {
            current_company: '=companySelector',
            current_contact: '=companyContact',
            edit_mode: '=canEdit'
        },
        templateUrl: '/record/segments/company_selector.html',
        link: {
            pre: function (scope) {
                var read_selected_company = function (company) {
                        if (company) {
                            if (scope.current_company && scope.current_company.key === company.key) {

                            } else {
                                if (company.new) {
                                    scope.current_company = company;
                                } else {
                                    scope.current_company = $firebase($rootScope.workspace.hack.get_companies(company.key));
                                }
                            }
                            _.defaults(scope.current_company, {
                                contacts: [{}]
                            });
                        }
                    },
                    add_http_to_url = function (url) {
                        if (!url) return url;
                        url = url.trim();
                        if (/^https?:\/\//.test(url)) {
                            return url;
                        } else {
                            return 'http://' + url;
                        }
                    };

                $rootScope.workspace.hack.get_companies().once('value', function (snap) {
                    var pick_name = function (item) {
                            return item.name;
                        },
                        data = snap.val();
                    scope.company_options = {
                        data: {
                            results: data ? _.values(data) : [],
                            text: 'name',
                        },
                        id: function (item) {
                            return item.key;
                        },
                        createSearchChoice: function (name) {
                            return {key: name, name:name, new: true, contacts: [{}],};
                        },
                        formatSelection: pick_name,
                        formatResult: pick_name,
                    };
                });

                $rootScope.$watch('path_base', function (path_base) {
                    scope.path_base = path_base;
                });

                scope.selected = {};

                scope.$watch('selected.company', read_selected_company);
                scope.$watch('current_company.url', function (value) {
                    if (value) {
                        scope.current_company.url = add_http_to_url(value);
                    }
                });
                scope.$watch('current_company.login_url', function (value) {
                    if (value) {
                        scope.current_company.login_url = add_http_to_url(value);
                    }
                });
                scope.$watch('current_company.contacts[0].url', function (value) {
                    if (value) {
                        scope.current_company.contacts[0].url = add_http_to_url(value);
                    }
                });

                scope.$watch('current_company', function (current) {
                    if (!scope.selected.company) {
                        scope.selected.company = current;
                    }
                });

                scope.selected.company = scope.current_company;
            },
        },
    };
},]);
