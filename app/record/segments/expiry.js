'use strict';

angular.module('bluedrive.record')

.directive('expiryInfo', [function () {
    return {
        scope: {
            expiry: '=expiryInfo',
            editable: '=canEdit'
        },
        templateUrl: '/record/segments/expiry.html',
        link: {
            pre: function (scope) {
                scope.set_renew = function (value) {
                    if (scope.editable) {
                        scope.expiry.renew = !!value;
                        if (value === 'term') {
                            scope.expiry.term = true;
                            scope.expiry.end = undefined;
                        } else {
                            scope.expiry.term = false;
                        }
                    }
                }

                var renewal_watcher = scope.$watch('expiry.renew_term', function (term) {
                    if (term) {
                        scope.span_length = moment.duration(term).asMonths() >= 12 ? 'year':'month';
                        renewal_watcher();
                    }
                });
                scope.show_lengths = false;
                scope.span_lengths = {
                    month: 'months',
                    year: 'years',
                };
                scope.span_length = 'year';
                scope.set_span_length = function (length) {
                    scope.span_length = length;
                    scope.show_lengths = false;
                };
            },
        },
    };
},]);
