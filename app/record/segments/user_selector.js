'use strict';

angular.module('bluedrive.record')

.directive('userSelector', ['data', '$rootScope', function (data, $rootScope) {
    return {
        scope: {
            user: '=userSelector',
        },
        templateUrl: '/record/segments/user_selector.html',
        link: {
            pre: function (scope) {
                var pick_name = function (user) {
                        return user.info && user.info.name;
                    },
                    pick_id = function (user) {
                       return user.key;
                    };
                $rootScope.workspace.hack.get_user_list().then(function (users) {
                    scope.select2_users = {
                        data: {
                            results: _.chain(users)
                                .map(function (value, key) {
                                    return _.extend({key: key}, value);
                                })
                                .values().value(),
                            text: pick_name },
                        id: pick_id,
                        formatSelection: pick_name,
                        formatResult: pick_name,
                    };
                    if (scope.user && scope.user.key) {
                        _.extend(scope.user, users[scope.user.key]);
                    }
                });
                scope.current = {
                    user: scope.user,
                };
                scope.$watch('current.user', function (user) {
                    scope.user = user;
                });
                scope.$watch('user', function (user) {
                    scope.current.user = user;
                });
            },
        },
    };
},]);
