'use strict';

angular.module('extras')

.directive('optionList', ['types', '$rootScope', function (types, $rootScope) {
    return {
        scope: {
            options: '=optionList',
            class: '@optionClass',
            edit_mode: '=canEdit',
            completeType: '@completeType',
            search: '=search',
            url: '@url',
        },
        templateUrl: '/record/segments/option_list.html',
        link: {
            pre: function (scope) {
                if (scope.completeType) {
                    $rootScope.workspace.hack.get_item(types.get(scope.completeType)).once('value', function (snap) {
                        var tag_options,
                            items = snap.val();
                        if (items) {
                            tag_options = _.chain(items).values().pluck('data').pluck('options').flatten().unique().compact().value();
                        } else {
                            tag_options = [];
                        }
                        scope.current_options = {
                            name: 'options',
                            local: _.map(tag_options, function (item) {
                                return {
                                    value: item,
                                    tokens: item.split(' '),
                                };
                            }),
                        };
                        if (!scope.$$phase) scope.$digest();
                    });
                }
                scope.base_url = (scope.url?scope.url:'') + '?text=';
                scope.add_tag = function () {
                    var new_val = scope.tag_text.value || scope.tag_text;
                    if (scope.options.indexOf(new_val) === -1) {
                        scope.options.push(new_val);
                    }
                    scope.tag_text = '';
                };
                scope.tag_text = '';
            },
        },
    };
},]);
