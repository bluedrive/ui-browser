'use strict';

angular.module('bluedrive.record')

.provider('types', ['cabinetRouteProvider', function (cabinetRouteProvider) {
    var get_location = function (type) {
            var split = type.id.split('.');
            if (split.length == 1) {
                return type.path + '/';
            }
            return get_location(types_base[split[0]]) + type.path + '/';
        },
        types_base = {
            register_drawer: function (drawer) {
                var base_url = get_location(drawer);

                drawer.location = base_url;
                types_base[drawer.id] = drawer;

                _.each(drawer.sub, function (type) {
                    type.location = get_location(type);
                    type.parent = types_base[type.id.split('.')[0]];

                    cabinetRouteProvider
                        .when(type.location, {
                            templateUrl: '/record/pages/record_list.html',
                            controller: 'ListController',
                            reloadOnSearch: false,
                            resolve: {
                                type: [function () {return type;}]
                            },
                        })
                        .when(type.location + ':name/', {
                            templateUrl: '/' + base_url + 'pages/' + type.path + '_detail.html',
                            controller: 'DetailController',
                            resolve: {
                                type: [function () {return type;}]
                            },
                        })
                        .when(type.location + ':name/history/', {
                            templateUrl: '/record/pages/history.html',
                            controller: 'History',
                            resolve: {
                                type: [function () {return type;}]
                            },
                        });
                });

                _.each(drawer.extra, function (type) {
                    type.location = get_location(type);
                    type.parent = types_base[type.id.split('.')[0]];

                    cabinetRouteProvider.when(type.location, {
                        templateUrl: '/' + base_url + 'pages/' + type.path + '.html',
                        controller: type.title,
                        resolve: {
                            type: [function () {return type;}]
                        },
                    });
                });

                cabinetRouteProvider
                    .when(base_url, {
                        templateUrl: '/record/pages/drawer.html',
                        controller: 'DrawerController',
                        resolve: {
                            drawer: [function () {return drawer;}]
                        },
                    })
                    .when(base_url + 'all/', {
                        templateUrl: '/record/reports/all.html',
                        controller: 'AllRecords',
                        reloadOnSearch: false,
                        resolve: {
                            drawer: [function () {return drawer;}]
                        },
                    })
                    .when(base_url + 'responsibilities/', {
                        templateUrl: '/record/reports/responsibilities.html',
                        controller: 'Responsibilities',
                        reloadOnSearch: false,
                        resolve: {
                            drawer: [function () {return drawer;}]
                        },
                    })
                    .when(base_url + 'renewals/', {
                        templateUrl: '/record/reports/renewals.html',
                        controller: 'Renewals',
                        reloadOnSearch: false,
                        resolve: {
                            drawer: [function () {return drawer;}]
                        },
                    });

                return drawer;
            },
            get_all: function () {
                return _.chain(this)
                    .omit(_.functions(this))
                    .omit('$get')
                    .values()
                    .pluck('sub')
                    .flatten(true)
                    .value();
            },
            get_drawers: function () {
                return _.chain(this)
                    .omit(_.functions(this))
                    .omit('$get')
                    .values()
                    .value();
            },
        };

    return _.extend(types_base, {
        $get: ['$q', function ($q) {
            return _.extend({
                load: function(workspace, name) {
                    var self = this,
                        names,
                        promises = [];
                    if (_.isString(name)) {
                        names = [name,];
                    } else if (_.isArray(name)) {
                        names = name;
                    } else {
                        names = _.chain(this)
                            .omit(_.functions(this))
                            .omit('$get')
                            .keys()
                            .value();
                    }
                    _.each(names, function (name) {
                        _.each(self[name].sub, function(type) {
                            var deferred = $q.defer();
                            promises.push(deferred.promise);
                            workspace.get_item(type).once('value', function (snap) {
                                type.data = snap.val();
                                deferred.resolve();
                            });
                        });
                    });
                    return $q.all(promises);
                },
                get: function (id) {
                    return _.findWhere(this.get_all(), {id: id});
                },
            }, types_base);
        }],
    });
}]);
