'use strict';

angular.module('bluedrive.record')

.filter('filter2', [function () {
    return function (items, match_function) {
        return _.filter(items, match_function);
    };
}])

.controller('Renewals', ['$scope', 'workspace', 'searchBind', 'types', '$route',
                function ($scope,   workspace,   searchBind,   types,   $route) {
    var type_list, load_types;
    searchBind($scope, 'search', ['text']);

    if ($route.current.locals.drawer) {
        $scope.drawer = $route.current.locals.drawer;
        type_list = types[$scope.drawer.id].sub;
        load_types = $scope.drawer.id;
    } else {
        type_list = types.get_all()
    }

    $scope.loading = true;
    types.load(workspace, load_types).then(function () {
        $scope.items = _.chain(type_list)
            .map(function (type) {
                return _.map(type.data, function (item) {
                    item.type = type.id;
                    return item;
                });
            })
            .flatten()
            .value();
        $scope.loading = false;
    });

    $scope.other = function (item) {
        item = item.data;
        return !item.expiry;
    };
    $scope.renews = function (item) {
        item = item.data;
        return item.expiry && item.expiry.end && item.expiry.renew;
    };
    $scope.expired = function (item) {
        item = item.data;
        return item.expiry && item.expiry.end && moment(item.expiry.end).isBefore() && !item.expiry.renew;
    };
    $scope.expires = function (item) {
        item = item.data;
        return item.expiry && item.expiry.end && moment(item.expiry.end).isAfter() && !item.expiry.renew;
    };
    $scope.term = function (item) {
        item = item.data;
        return item.expiry && !item.expiry.end && item.expiry.renew;
    };
}]);
