'use strict';

angular.module('bluedrive.record')

.controller('Responsibilities', ['$scope', 'workspace', 'drawer', 'searchBind', 'data', 'types',
                        function ($scope,   workspace,   drawer,   searchBind,   data,   types) {
    var load_types_promise = types.load(workspace, drawer.id);
    $scope.loading = true;
    workspace.get_users().then(function (users) {
        $scope.users = users;
        load_types_promise.then(function () {
            $scope.loading = false;
        });
    });

    $scope.search = {};
    searchBind($scope, 'search', ['people', 'text']);

    $scope.set_person = function (data) {
        $scope.search.people = data;
    };
    $scope.drawer = drawer;
    $scope.types = drawer.sub;
}]);
