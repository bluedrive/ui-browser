'use strict';

angular.module('bluedrive.record')

.controller('AllRecords', ['$scope', 'workspace', 'drawer', 'searchBind', 'types', function ($scope, workspace, drawer, searchBind, types) {
    searchBind($scope, 'search', ['text']);
    $scope.loading = true;
    types.load(workspace, drawer.id).then(function () {
        $scope.loading = false;
    });
    $scope.drawer = drawer;
    $scope.types = drawer.sub;
}]);
