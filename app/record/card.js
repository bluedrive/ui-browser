angular.module('bluedrive.record')

.directive('contactCard', ['data', '$rootScope', '$firebase', function (data, $rootScope, $firebase) {return {
    scope: {
        user_key: '=contactCard',
        email_subject: '@emailSubject',
        hide_name: '=hideName',
    },
    templateUrl: '/record/contact_card.html',
    link: function (scope) {
        var set_user_from_key = function(user_key) {
                if (!user_key) {
                    scope.user = undefined;
                } else if (_.isString(user_key)) {
                    scope.user = $firebase($rootScope.workspace.hack.get_user_ref(user_key));
                } else {
                    scope.user = user_key;
                }
            };
        $rootScope.$watch('path_base', function (path_base) {
            scope.path_base = path_base;
        });
        scope.$watch('user_key', set_user_from_key);
        set_user_from_key(scope.user_key);
    }
}}])
.directive('companyCard', ['data', '$rootScope', '$firebase', function (data, $rootScope, $firebase) {return {
    scope: {
        company_key: '=companyCard',
        contact: '=contact',
        account_key: '=account',
        email_subject: '@emailSubject',
        hide_name: '=hideName',
    },
    templateUrl: '/record/company_card.html',
    link: function (scope) {
        var set_company_from_key = function(company_key) {
            if (!company_key) {
                scope.company = undefined;
            } else if (company_key.key) {
                scope.company = company_key;
            } else {
                scope.company = $firebase(data.get_companies(company_key));
            }
        };
        $rootScope.$watch('path_base', function (path_base) {
            scope.path_base = path_base;
        });
        scope.$watch('company_key', set_company_from_key);
        set_company_from_key(scope.company_key);

        var set_account_from_key = function(account_key) {
            if (!account_key) {
                scope.account = undefined;
            } else if (account_key.key) {
                scope.account = account_key;
            } else {
                if (scope.company && scope.company.accounts) {
                    scope.account = scope.company.accounts[account_key];
                }
            }
        };
        scope.$watch('account_key', set_account_from_key);
        set_account_from_key (scope.account_key);
    }
}}]);
