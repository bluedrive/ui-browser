'use strict';

angular.module('bluedrive.record', ['bluedrive.base', 'textAngular', 'extras'])

.config(['cabinetRouteProvider', function (cabinetRouteProvider) {
    cabinetRouteProvider
        .when('add/', {
            templateUrl: '/record/pages/add.html',
            controller: 'AddRecord',
            reloadOnSearch: false,
        });
}]);
