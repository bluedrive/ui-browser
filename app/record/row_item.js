'use strict';

angular.module('bluedrive.record')

.directive('rowItem', ['types', 'data', '$rootScope', function (types, data, $rootScope) { return {
    scope: {
        item: '=rowItem',
        textType: '@rowType',
        hide_user: '=hideUser',
        url: '@url',
    },
    templateUrl: '/record/row_item.html',
    link: function (scope) {
        var set_type = function (type) {
                var drawer, type_name, split_type;
                if (type) {
                    scope.type = types.get(type);
                    split_type = type.split('.', 2);
                    drawer = split_type[0];
                    type_name = split_type[1];
                    scope.template = '/' + drawer + '/row_items/' + type_name + '.html';
                } else {
                    scope.template = null;
                    scope.type = null;
                }
            };
        scope.workspace = $rootScope.workspace;
        scope.switch_folder = $rootScope.switch_folder;
        scope.path_base = $rootScope.path_base;
        $rootScope.workspace.hack.get_users().then(function (users) {
            scope.users = users;
        });

        scope.$watch('item', function (item) {
            scope.accounts = _.unique([item.data.account, item.data.account2]);
        });

        scope.$watch('textType', set_type);
        set_type(scope.textType);
    },
}}]);
