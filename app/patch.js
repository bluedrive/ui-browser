// pick in underscore 1.7 ?
_.pick = function(obj, iterator, context) {
    var result = {};
    if (_.isFunction(iterator)) {
        for (var key in obj) {
            var value = obj[key];
            if (iterator.call(context, value, key, obj)) result[key] = value;
        }
    } else {
        var keys = Array.prototype.concat.apply([], Array.prototype.slice.call(arguments, 1));
        for (var i = 0, length = keys.length; i < length; i++) {
            var key = keys[i];
            if (key in obj) result[key] = obj[key];
        }
    }
    return result;
};
