'use strict';

angular.module('bluedrive.cabinet')

.controller('Company', ['$scope', '$routeParams', 'workspace', 'data', 'types', function ($scope, $routeParams, workspace, data, types) {
    $scope.company = $firebase(workspace.get_companies($routeParams.name));
    $scope.loading = true;
    types.load(workspace).then(function () {
        $scope.loading = false;
    });
    $scope.types = types.get_all();
}]);
