'use strict';

angular.module('bluedrive.record')

.controller('AllCabinetRecords', ['$scope', 'workspace', 'searchBind', 'types', function ($scope, workspace, searchBind, types) {
    searchBind($scope, 'search', ['text']);

    $scope.loading = true;
    types.load(workspace).then(function () {
        $scope.loading = false;
    });

    $scope.types = types.get_all();
}]);
