'use strict';

angular.module('bluedrive.cabinet')

.controller('User', ['$scope', 'workspace', '$routeParams', 'data', 'types', function ($scope, workspace, $routeParams, data, types) {
    var load_types_promise = types.load(workspace);
    $scope.loading = true;
    workspace.get_users($routeParams.name).then(function(user) {
        $scope.user = user;
        load_types_promise.then(function () {
            $scope.loading = false;
        })
    });
    $scope.user_key = $routeParams.name;
    $scope.types = types.get_all();
}]);
