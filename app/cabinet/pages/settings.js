'use strict';

angular.module('bluedrive.record')

.controller('CabinetSetting', ['$scope', 'workspace', 'userTracking', '$firebase',
        function ($scope, cabinet, userTracking, $firebase) {
    $scope.cabinet = $firebase(cabinet.details_ref);
    cabinet.get_users().then(function (users) {
        $scope.users = users;
    });

    $scope.new_folders = [];

    $scope.save = function () {
        $scope.cabinet.$save();

        _.each($scope.new_folders, function (folder) {
            if (!folder.name) return;
            cabinet.create_folder(folder.name);
            userTracking.event.section_created();
        });

        $scope.new_folders = [];
    };
}]);
