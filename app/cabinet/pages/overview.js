"use strict";

angular.module('bluedrive.cabinet')

.controller('CabinetOverview', ['$scope', 'types', 'workspace', '$location',
        function ($scope, types, workspace, $location) {
    workspace.links_ref.on('value', function (snap) {
         $scope.links = snap.val();
         $scope.$apply();
    });
    $scope.drawers = types.get_drawers();
    $scope.run_search = function (url) {
        $location.url(url);
    };
}]);
