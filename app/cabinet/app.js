'use strict';

angular.module('bluedrive.cabinet', ['bluedrive.technical', 'bluedrive.financial', 'bluedrive.legal'])

.config(['cabinetRouteProvider', function (cabinetRouteProvider) {
    cabinetRouteProvider
        .when('settings/', {
            templateUrl: '/cabinet/pages/settings.html',
            controller: 'CabinetSetting',
        })
        .when('all/', {
            templateUrl: '/cabinet/pages/all_records.html',
            controller: 'AllCabinetRecords',
            reloadOnSearch: false,
        })
        .when('renewals/', {
            templateUrl: '/record/reports/renewals.html',
            controller: 'Renewals',
            reloadOnSearch: false,
        })
        .when('users/:name/', {
            templateUrl: '/cabinet/pages/user.html',
            controller: 'User',
        })
        .when('companies/:name/', {
            templateUrl: '/cabinet/pages/company.html',
            controller: 'Company',
        });
}])
.run(['tours', 'userTracking', '$rootScope', '$timeout', function (tours, userTracking, $routeScope, $timeout) {
    var tour = tours.create_tour('intro', {
        defaults: {
            showCancelLink: true,
            classes: 'shepherd-theme-arrows',
            scrollTo: true,
            buttons: [
                {
                    text: 'Next',
                    action: function () {tour.next()}
                }
            ]
        }
    });
    tour.addStep('welcome', {
        title: 'Welcome to BlueDrive!',
        text: 'Thanks for trying BlueDrive! We want you to have the best possible<br>\
            experience and built this quick tour to make sure you know about <br>\
            the main features of the product.',
        buttons: [
            {
                text: 'Start the Tour',
                action: function () {
                    $routeScope.$broadcast('show_sidebar', true);
                    $timeout(tour.next, 300);
                }
            }
        ]
    });
    tour.addStep('cabinet', {
        title: 'Cabinets',
        text: 'Select your cabinet from here.<br><br>\
            Create a new cabinet for each investment or company.',
        attachTo: '.tour-cabinet-list right',
        buttons: [
            {
                text: 'Next',
                action: function () {
                    $routeScope.$broadcast('show_sidebar', false);
                    tour.next();
                }
            }
        ]
    });
    tour.addStep('drawer', {
        title: 'Drawers',
        text: 'Records are stored in independent drawers of the cabinet.<br><br>\
            Click on a drawer title to open a drawer.',
        attachTo: '.tour-drawer-title right',
    });
    tour.addStep('record', {
        title: 'Records',
        text: 'Add a Record in that drawer, be it technical, financial or legal. <br><br>\
            Each record brings together information about the operation of the <br>\
            business, including the assets, people, their contact information, <br>\
            responsibilities, renewal dates, files, websites, documents and emails. <br><br>\
            Literally any digital file, email or information store with a unique URL <br>\
            can be linked in BlueDrive.',
        attachTo: '.tour-drawer-add-record right',
    });
    tour.addStep('reports', {
        title: 'Reports',
        text: 'Get a listing of which team members are responsible for what <br>\
            asset, liability, commitment, and renewal.',
        attachTo: '.tour-drawer-reports right',
        buttons: [
            {
                text: 'Next',
                action: function () {
                    userTracking.event.completed_intro_tour();
                    tour.next();
                }
            }
        ]
    });
    tour.addStep('end', {
        title: 'Need Additional Help?',
        text: 'At any time, click the &nbsp;<span class="glyphicon glyphicon-question-sign"></span>&nbsp; mark to correspond with BlueDrive support.',
        attachTo: '#help top',
        buttons: [
            {
                text: 'Finish Tour',
                action: function () {
                    tour.complete();
                }
            }
        ]
    });
    }]);
