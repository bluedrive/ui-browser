'use strict';

angular.module('bluedrive.financial')

.directive('accountList', [function () {
    return {
        templateUrl: '/financial/account_list.html',
        scope: {
            accounts: '=accountList',
            edit_mode: '=edit',
        },
        link: function (scope) {
            if (!scope.accounts) {
                scope.accounts = [];
            }
        },
    };
}]);
