'use strict';

angular.module('bluedrive.financial', ['ngRoute', 'ui.select2', 'templates', 'bluedrive.record'])

.config(['typesProvider', function (typesProvider) {
    typesProvider.register_drawer({
        id: 'financial',
        title: 'Financial',
        path: 'financial',
        sub: [{
            title: 'Assets',
            add_title: 'Asset',
            id: 'financial.asset',
            path: 'assets',
            database_name: 'assets',
            input_title: 'Asset Name',
            pattern: /^.+$/,
            common_relations: ['Mortgage Broker', 'Borrower of Funds', 'Legal Representative to Borrower', 'Legal Representative to Lender', 'Insurance Agency', 'Property Manager'],
        }, {
            title: 'Liabilities',
            add_title: 'Liability',
            id: 'financial.liability',
            path: 'liabilities',
            database_name: 'liabilities',
            input_title: 'Liability Name',
            pattern: /^.+$/,
            common_relations: ['Mortgage Broker', 'Borrower of Funds', 'Legal Representative to Borrower', 'Legal Representative to Lender', 'Insurance Agency', 'Property Manager'],
        }, {
            title: 'Bank Accounts',
            add_title: 'Bank Account',
            id: 'financial.bank',
            path: 'banking',
            database_name: 'banks',
            input_title: 'Account Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'E-Commerce Accounts',
            add_title: 'E-Commerce Account',
            id: 'financial.e_commerce',
            path: 'e_commerce',
            database_name: 'e_commerce',
            input_title: 'Account Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'Compliance',
            add_title: 'Compliance Record',
            id: 'financial.compliance',
            path: 'compliance',
            database_name: 'financial_compliance',
            input_title: 'Compliance Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'Financial Meeting Minutes',
            add_title: 'Financial Meeting Name',
            id: 'financial.meeting',
            path: 'meetings',
            database_name: 'financial_meetings',
            input_title: 'Financial Meeting Name',
            pattern: /^.+$/,
            common_relations: [''],
        }, {
            title: 'Financial Reports',
            add_title: 'Financial Report',
            id: 'financial.report',
            path: 'reports',
            database_name: 'financial_reports',
            input_title: 'Financial Report',
            pattern: /^.+$/,
            common_relations: [''],
        },],
    });
}]);
