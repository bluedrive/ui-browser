'use strict';

var angular_redirect = function (base_url) {
    return function (req, res, next) {
        var is_route = /^.*\.\w+$/;
        if ('GET' != req.method && 'HEAD' != req.method) return next();
        if (is_route.test(require('url').parse(req.url).pathname)) return next();
        res.statusCode = 302;
        res.setHeader('Location', base_url + '#' + req.url);
        res.end();
    }
}

module.exports = function(grunt) {
    var conf = require('./conf');

    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks("grunt-concurrent");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-contrib-connect");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-htmlmin");
    grunt.loadNpmTasks("grunt-contrib-jade");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-cache-breaker");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-usemin");
    grunt.loadNpmTasks("grunt-karma");

    grunt.initConfig({
        connect: {
            options: {
                hostname: '*',
            },
            development: {
                options: {
                    middleware: function (connect) {
                        return [
                            require('connect-livereload')(),
                            connect.static(__dirname + '/.tmp'),
                            connect.static(__dirname + '/bower_components'),
                            angular_redirect('/'),
                        ];
                    },
                },
            },
            dist : {
                options: {
                    middleware: function (connect) {
                        return [
                            connect.static(__dirname + '/dist'),
                            angular_redirect('/'),
                        ];
                    },
                },
            },
        },
        watch: {
            styles: {
                files: ['app/**/*.less'],
                tasks: ['less:development'],
            },
            jade: {
                files: ['app/**/*.jade'],
                tasks: ['jade:development'],
            },
            javascript: {
                options: {livereload: true},
                files: ['app/**/*.js'],
                tasks: ['copy:javascript'],
            },
            css_livereload: {
              options: { livereload: true },
              files: ['.tmp/**/*.css',],
            },
            html_livereload: {
              options: { livereload: true },
              files: ['.tmp/**/*.html'],
            }
        },
        less: {
            options: {
                paths: ['app/', 'bower_components/']
            },
            development: {
                options: {
                    sourceMap: true,
                },
                files: {
                    '.tmp/style.css': 'app/base.less'
                }
            },
            dist: {
                options: {
                    cleancss: true,
                },
                files: {
                    'dist/style.css': 'app/base.less'
                }
            }
        },
        jade: {
            options: {
                data: function (dest, src) {
                    if (/index.jade$/.test(src)) {
                        return {
                            debug: !!this.debug_mode,
                            cdn_files: conf.cdn_files,
                            library_files: conf.library_files(),
                            application_files: conf.application_files('app'),
                        };
                    }
                },
            },
            development: {
                options: {
                    basedir: 'app',
                    pretty: true,
                    debug_mode: true,
                },
                files: [{
                    expand: true,
                    cwd: 'app',
                    dest: '.tmp',
                    src: ['**/*.jade', '!**/*_mixin.jade'],
                    ext: '.html'
                }],
            },
            dist: {
                options: {
                    basedir: 'app',
                    pretty: true
                },
                files: [{
                    expand: true,
                    cwd: 'app',
                    dest: '.tmp',
                    src: ['**/*.jade', '!**/*_mixin.jade'],
                    ext: '.html'
                }],
            },
        },
        copy: {
            javascript: {
                files: [
                    {expand: true, cwd: 'app/', src: ['**/*.js'], dest: '.tmp/', filter: 'isFile'},
                ]
            },
            concat: {
                files: [
                    {expand: true, cwd: '.tmp/concat/', src: ['**/*.js'], dest: 'dist/', filter: 'isFile'},
                ]
            },
            assetsdev: {
                files: conf.copy_assets('.tmp/'),
            },
            assetsdist: {
                files: conf.copy_assets('dist/'),
            },
        },
        htmlmin: {
            dist: {
                files: {'dist/index.html': '.tmp/index.html',},
                options: {
                    collapseBooleanAttributes:      true,
                    collapseWhitespace:             true,
                    //removeAttributeQuotes:          true,
                    //removeComments:                 true,
                    //removeEmptyAttributes:          true,
                    removeRedundantAttributes:      true,
                    removeScriptTypeAttributes:     true,
                    removeStyleLinkTypeAttributes:  true
                },
            },
        },
        useminPrepare: {
            html: '.tmp/index.html',
        },
        usemin: {
            html: '.tmp/index.html',
            options: {
                assetsDirs: ['dist']
            }
        },
        ngtemplates:  {
            dist: {
                cwd: '.tmp',
                src: '*/**/*.html',
                dest: '.tmp/templates.js',
                options:  {
                    prefix: '/',
                    module: 'templates',
                    htmlmin:  '<%= htmlmin.dist.options %>',
                },
            },
        },
        concat: {
            templates: {
                files: {
                    '.tmp/concat/app.js': ['.tmp/concat/app.js', '.tmp/templates.js'],
                }
            }
        },
        uglify: {
            dist: {
                mangle: true,
                compress: true,
                files: [
                    {expand: true, cwd: '.tmp/concat/', src: ['**/*.js'], dest: 'dist/', filter: 'isFile'},
                ],
            },
        },
        cachebreaker: {
            dist: {
                options: {
                    match: ['app.js', 'components.js', 'style.css', ],
                },
                files: {
                    src: ['dist/index.html']
                }
            }
        },
        jshint: {
            options: {
                strict: false,
                laxbreak: true,
                debug: true,
                globals: {
                    angular: true,
                    $: true,
                    _: true
                }
            },
          all: 'app'
        },
        concurrent: {
            server: [
                'less:development',
                'jade:development',
                'copy:javascript',
                'copy:assetsdev',
            ],
            build: [
                'less:dist',
                'jade:dist',
                'copy:assetsdist',
            ],
            target: {
                tasks: ['watch'],
                options: {
                  logConcurrentOutput: true
                }
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        'dist/*',
                    ]
                }]
            },
            server: '.tmp'
        },
        karma: {
            unit: {
                configFile: 'tests/karma-unit.conf.js',
            },
        },
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            grunt.task.run(['build', 'connect:dist', 'watch']);
        } else {
            grunt.task.run([
                'clean:server',
                'concurrent:server',
                'connect:development',
                'watch'
            ]);
        }
    });


    grunt.registerTask('test', [
        'karma:unit',
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        //'jshint',
        'concurrent:build',
        'useminPrepare:html',
        'concat:generated',
        'ngtemplates:dist',
        'concat:templates',
        'uglify:dist',
        'usemin:html',
        'htmlmin:dist',
        'cachebreaker:dist',
    ]);

    grunt.registerTask('default', ['build',]);
};
