var glob = require('glob')

module.exports = {
    cdn_files: [
        'https://cdn.firebase.com/v0/firebase.js',
        'https://cdn.firebase.com/js/simple-login/1.3.0/firebase-simple-login.js',
        'https://static.intercomcdn.com/intercom.v1.js',
        'https://www.dropbox.com/static/api/1/dropins.js',
        'https://apis.google.com/js/client.js',
    ],
    library_files: function (prefix) {
        prefix = prefix !== undefined ? prefix : '';
        var files = [
            'underscore/underscore.js',
            'cryptojs/lib/Crypto.js',
            'cryptojs/lib/MD5.js',
            'jquery/jquery.js',
            'momentjs/moment.js',
            'angular/angular.js',
            'select2/select2.js',
            'typeahead.js/dist/typeahead.js',
            'angular-cookies/angular-cookies.js',
            'angular-fire/angularfire.js',
            'angular-route/angular-route.js',
            'angular-ui-select2/src/select2.js',
            'tether/tether.js',
            'shepherd.js/shepherd.js',
            'ng-file-upload/angular-file-upload.js',
            'angular-sanitize/angular-sanitize.js',
            'textAngular/textAngular.js',
        ];
        if (prefix !== undefined) {
            return files.map(function (file) {
                return prefix + file;
            });
        } else {
            return files;
        }
    },
    application_glob: function (prefix) {
        prefix = prefix !== undefined ? prefix : '';
        return [
            '**/app.js',
            '**/!(app).js',
        ].map(function (glob) {
            return prefix + glob;
        });
    },
    application_files: function (cwd) {
        return this.application_glob()
            .reduce(function (list, pattern) {
                return list.concat(glob.sync(pattern, {cwd: cwd}));
            }, []);
    },
    copy_assets: function (dest) {
        return [
            {expand: true, cwd: 'app/', src: ['**/*.{png,gif}'], dest: dest, filter: 'isFile'},
            {expand: true, cwd: 'bower_components/bootstrap/dist/', src: ['fonts/*'], dest: dest, filter: 'isFile'},
            {expand: true, cwd: 'bower_components/fontawesome/', src: ['fonts/*'], dest: dest, filter: 'isFile'},
            {expand: true, cwd: 'bower_components/select2/', src: ['*.{png,gif}'], dest: dest, filter: 'isFile'},
        ];
    }
};
