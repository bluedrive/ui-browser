# Setup Node.js 
```
 sudo add-apt-repository ppa:chris-lea/node.js
 sudo apt-get update
 sudo apt-get install nodejs
```

# Setup growl
```
sudo add-apt-repository -y ppa:mattn/growl-for-linux
sudo apt-get -y update
sudo apt-get -y install growl-for-linux
```

# Install Grunt
```
sudo npm install grunt-cli
```

# Install Dependencies
```
 npm install
 bower install
```

# Run
```
 grunt server
```

# Build
```
 grunt build
```
