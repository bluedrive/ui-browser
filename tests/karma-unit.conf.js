var sharedConfig = require('./karma-shared.conf');

module.exports = function (config) {
    var conf = sharedConfig();

    conf.files = conf.files.concat([
        'bower_components/angular-mocks/angular-mocks.js',
        'tests/mocha.conf.js',
        'tests/unit/**/*!(_test).js',
        'tests/unit/**/*_test.js',
    ]);
    config.set(conf);
};
