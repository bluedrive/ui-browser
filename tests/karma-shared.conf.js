module.exports = function () {
    var conf = require('../conf');
    return {
        basePath: '../',
        frameworks: ['mocha'],
        reporters: ['progress'],
        browsers: ['PhantomJS'],
        autoWatch: true,
        progress: ['progress', 'growl'],

        files: []
            .concat(conf.cdn_files)
            .concat(conf.library_files('bower_components/'))
            .concat(conf.application_glob('app/'))
            .concat([
                'bower_components/chai/chai.js',
                'bower_components/sinon/lib/sinon.js',
                'bower_components/sinon-chai/lib/sinon-chai.js',
                'tests/lib/chai-setup.js',
            ]),
    };
};
