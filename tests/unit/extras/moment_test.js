describe("Unit: Angular moment filters", function() {
    beforeEach(angular.mock.module('extras'));

    it('should have a filters', inject(function($filter) {
        expect($filter('from')).not.to.equal(null);
        expect($filter('duration')).not.to.equal(null);
        expect($filter('every')).not.to.equal(null);
        expect($filter('beforeNow')).not.to.equal(null);
    }));


});

describe("Unit: Angular moment directives", function() {
    beforeEach(angular.mock.module('extras'));

    it('should have a filters', inject(function($filter) {
    }));
});
